const express = require("express");
var bodyParser = require("body-parser");
const db = require("./config/db");

const app = express();
db(app);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const router = require("./router/router");
app.use("/", router);
