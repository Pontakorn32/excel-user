const readXlsxFile = require("read-excel-file/node");
var excel = require("excel4node");

const utils = require("../utils/extendtion");
const PermissionKeys = require("../models/permissionKey");

const controller = {};
var workbook = new excel.Workbook();

controller.formatExcelToVCFController = async (req, res) => {
  let data = req.body;
  if (!data.key) {
    res.status(500).send({ message: "กรุณาใส่ key เพื่อใช้งาน" });
  }
  let key = await PermissionKeys.findOne({ key: data.key }).exec();
  if (!key) {
    res.status(500).send({ message: "ไม่สามารถใช้ key นี้ได้" });
  }

  let sheetPage = 1;
  let indexInsertExcel = 0;
  readXlsxFile(`./input/${data.fileName}.xlsx`).then(async (response) => {
    let numberStore = [];
    let amountFormat = 0;
    let remaining;

    response.forEach((e) => {
      let number = e[data.row];
      if (String(number).length > 8 && String(number).length < 11) {
        numberStore.push(number);
      }
    });

    var worksheet = workbook.addWorksheet(`Sheet${sheetPage}`);
    if (key.amount_per_number >= numberStore.length) {
      amountFormat = numberStore.length;
      remaining = key.amount_per_number - numberStore.length;
      numberStore.forEach((res) => {
        let formatPhoneNumber = utils.formatNumber(res);
        if (formatPhoneNumber != null) {
          let lenPhoneNumber = formatPhoneNumber.split("");
          if (
            (parseInt(lenPhoneNumber[0]) != 0 && lenPhoneNumber.length == 9) ||
            (parseInt(lenPhoneNumber[0]) == 0 && lenPhoneNumber.length == 10)
          ) {
            worksheet.cell(indexInsertExcel + 1, 1).string(formatPhoneNumber);
            indexInsertExcel++;
            if (indexInsertExcel == 500) {
              // console.log(sheetPage);
              indexInsertExcel = 0;
              sheetPage = parseInt(sheetPage) + 1;
              worksheet = workbook.addWorksheet(`Sheet${sheetPage}`);
            }
          }
        }
      });
      updateAmountKey(data.key, remaining);
    } else {
      amountFormat = key.amount_per_number;
      remaining = 0;
      for (let i = 0; remaining < key.amount_per_number; i++) {
        let number = response[i][data.row];
        let formatPhoneNumber = utils.formatNumber(number);
        if (formatPhoneNumber != null) {
          let lenPhoneNumber = formatPhoneNumber.split("");
          if (
            (parseInt(lenPhoneNumber[0]) != 0 && lenPhoneNumber.length == 9) ||
            (parseInt(lenPhoneNumber[0]) == 0 && lenPhoneNumber.length == 10)
          ) {
            worksheet.cell(indexInsertExcel + 1, 1).string(formatPhoneNumber);
            indexInsertExcel++;
            remaining++;
            if (indexInsertExcel == 500) {
              // console.log(sheetPage);
              indexInsertExcel = 0;
              sheetPage = parseInt(sheetPage) + 1;
              worksheet = workbook.addWorksheet(`Sheet${sheetPage}`);
            }
          }
        }
      }
      deleteKey(data.key);
    }
    workbook.write(
      `./format/${data.fileName}-format.xlsx`,
      async function (err, state) {
        if (err) {
          res.status(500).send({
            error: `Unable to parse "${data.pathInput}": "${inputFileExtension}" is not a supported format:`,
            err,
          });
        } else {
          const inputFileExtension = utils.getFileExtension(data.pathInput);
          if (inputFileExtension == ".xlsx" || inputFileExtension == ".xls") {
            await utils.parseXlsxFileToContacts(data.pathInput);
            res.status(200).send({
              message: `ข้อมูลทั้งหมดจำนวน ${response.length} ข้อมูล จำนวนโควต้าที่ใช้ไป ${amountFormat} เบอร์ จำนวนโควต้าคงเหลือ ${remaining} เบอร์`,
              state: state,
            });
          } else {
            res.status(500).send({
              error: `Unable to parse "${data.pathInput}": "${inputFileExtension}" is not a supported format`,
            });
          }
        }
      }
    );
  });
};

async function deleteKey(key) {
  await PermissionKeys.deleteOne({
    key: key,
  });
}

async function updateAmountKey(key, amount) {
  await PermissionKeys.updateOne(
    {
      key: key,
    },
    { amount_per_number: amount }
  );
}

module.exports = controller;
