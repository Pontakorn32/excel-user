const mongoose = require("mongoose");
const Schema = mongoose.Schema;
mongoose.set("debug", true);

const permissionKey = new Schema(
  {
    key: {
      type: String,
      required: true,
    },
    amount_per_number: {
      type: Number,
      required: true,
    },
  },
  {
    collection: "permissionKey",
  }
);

const permissionKeyModel = mongoose.model("permissionKey", permissionKey);

module.exports = permissionKeyModel;
