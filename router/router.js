const controller = require("../controller/excelController");

const express = require("express");

const router = express.Router();

router.post("/format-file", controller.formatExcelToVCFController);

module.exports = router;
