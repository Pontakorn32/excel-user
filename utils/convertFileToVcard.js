/**
 * Format a telephone number.
 * @param {string} telephoneNumber - A telephone number to format.
 * @returns {string} The telephone number, formatted.
 */
function formatTelephoneNumber(telephoneNumber) {
  return telephoneNumber;
}

/**
 * Formats the current date as a shortened, normalized ISO string to be used as the value of the REV field of a vCard.
 * @returns {string} The current date, formatted as a shortened, normalized ISO string.
 */
function formatCurrentDateAsISO() {
  try {
    const currentDate = new Date();
    const currentIsoDate = currentDate.toISOString();
    const normalizedIsoDate = currentIsoDate.replace(/([-:]|\.\d*)+/g, "");
    return normalizedIsoDate;
  } catch (error) {
    console.error("Unable to format current ISO date:", error);
  }
}

function convertContactToVcard(contact) {
  let vcardString = "BEGIN:VCARD\n" + "VERSION:4.0\n";
  if (contact.firstName || contact.lastName) {
    vcardString +=
      `N:${contact.lastName ? contact.lastName : ""};${
        contact.firstName ? contact.firstName : ""
      };;;\n` + `FN:${contact.firstName} ${contact.lastName}\n`;
  }
  if (contact.telephoneNumber) {
    vcardString += `TEL;TYPE=cell:${formatTelephoneNumber(
      contact.telephoneNumber
    )}\n`;
  }
  if (contact.emailAddress) {
    vcardString += `EMAIL:${contact.emailAddress}\n`;
  }
  vcardString += `REV:${formatCurrentDateAsISO()}\n` + "END:VCARD\n";
  return vcardString;
}

module.exports = convertContactToVcard;
