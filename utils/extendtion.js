const path = require("path");
const fs = require("fs");
const stream = require("stream");
const xlsx = require("xlsx");
const parseCsvDataStreamToContacts = require("./parseCsvData");
const convertContactToVcard = require("./convertFileToVcard");
const createOutputFilePath = require("./output");
const utils = {};

utils.formatNumber = (number) => {
  let replaceNumber = String(number).replace("-", "");
  let replaceFreeSpaces = replaceNumber.split(" ").join("");
  let lenNumber = replaceFreeSpaces.length;
  if (
    number != null &&
    lenNumber > 8 &&
    lenNumber < 11 &&
    !isNaN(replaceFreeSpaces)
  ) {
    return replaceFreeSpaces;
  }
  return null;
};

utils.getFileExtension = (file) => {
  try {
    const fileExtension = path.extname(file);
    return fileExtension;
  } catch (error) {
    console.error(`Unable to determine the extension of "${file}":`, error);
  }
};

utils.parseXlsxFileToContacts = (inputFile) => {
  return new Promise((resolve, reject) => {
    try {
      let buffers = [];
      let contacts;
      let contactsAsVcards = "";
      fs.createReadStream(inputFile)
        .on("data", (data) => {
          buffers.push(data);
        })
        .on("error", (error) => {
          console.error(`Unable to read file "${inputFile}":`, error);
        })
        .on("end", async () => {
          const buffer = Buffer.concat(buffers);
          const workbook = xlsx.read(buffer, { type: "buffer" });
          // console.log("SheetNames", workbook.SheetNames);
          workbook.SheetNames.forEach(async (data, index) => {
            const allWorksheet = workbook.Sheets[data];
            // convert the entire worksheet to a single csv string
            const csvData = xlsx.utils.sheet_to_csv(allWorksheet, {
              raw: true,
            });
            // convert the csv string into a stream
            var csvDataStream = new stream.Readable();
            csvDataStream.push(csvData);
            csvDataStream.push(null);
            // csvDataStream.pipe(process.stdout);
            // console.log(csvDataStream);

            contacts = await parseCsvDataStreamToContacts(
              csvDataStream,
              ","
            ).catch((e) => {
              console.log(e);
            });
            // resolve(contacts);
            // console.log("contacts", contacts);
            contacts.forEach((contact) => {
              contactsAsVcards += convertContactToVcard(contact);
            });
            fs.writeFileSync(
              createOutputFilePath(index + 1, inputFile),
              contactsAsVcards
            );
          });
        });
      resolve(contacts);
      resolve(contactsAsVcards);
    } catch (error) {
      reject(`Unable to parse input .xls(x) file:`, error);
    }
  });
};

module.exports = utils;
