const path = require("path");

function createOutputFilePath(i, pathInput) {
  const outputDirectory = "./output";
  const outputFileName = getFileBaseName(pathInput) + "_" + i;
  const outputFilePath = `${path.join(outputDirectory, outputFileName)}.vcf`;
  return outputFilePath;
}

function getFileBaseName(file) {
  try {
    const fileBaseName = path.basename(file).split(".")[0];
    return fileBaseName;
  } catch (error) {
    console.error(`Unable to determine the extension of "${file}":`, error);
  }
}

module.exports = createOutputFilePath;
