const csvParse = require("csv-parse");
class Contact {
  /**
   * Represents a single contact.
   * @constructor The email address of the contact.
   * @param {string} telephoneNumber - The telephone number of the contact.
   */
  constructor(telephoneNumber) {
    this.telephoneNumber = telephoneNumber;
  }
}

function stripUnwantedRows(rows) {
  const startRow = 0; // shift from 1-based to 0-based
  const endRow = rows.length;
  let wantedRows = [];
  if (endRow) {
    wantedRows = rows.slice(startRow, endRow);
  } else {
    wantedRows = rows.slice(startRow);
  }
  return wantedRows;
}
async function parseCsvDataStreamToContacts(csvDataStream, delimiter) {
  return new Promise((resolve, reject) => {
    try {
      let contacts = [];
      csvDataStream
        .pipe(csvParse({ delimiter }))
        .on("data", (contact) => {
          contacts.push(new Contact(...contact));
        })
        .on("error", (error) => {
          console.error(`Unable to read file `, error);
        })
        .on("end", () => {
          contacts = stripUnwantedRows(contacts);
          resolve(contacts);
        });
    } catch (error) {
      reject(`Unable to parse CSV data stream to contacts:`, error);
    }
  });
}

module.exports = parseCsvDataStreamToContacts;
